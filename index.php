<?php

set_time_limit(180);
ignore_user_abort(true);
ini_set('output_buffering', 0);
ini_set('display_errors', '0');
ini_set('date.timezone', 'Etc/GMT+8'); //--Is America/Vancouver With DTS AKA winter time in BC
ini_set('memory_limit','512M');

//##############################################################
//##############################################################-- Start Variable Fetching
//##############################################################

$system_timestamp=date('YmdHis');
$system_docroot=$_SERVER["DOCUMENT_ROOT"];
$system_apikey=makesafe($_GET['key']);

echo "<div>Welcome to the Hey.Café Bot</div>";

//##############################################################
//##############################################################-- Functions
//##############################################################

function makesafe($d){
	$d = str_replace("\t","~~",$d);
	$d = str_replace("\r","",$d);
	$d = str_replace("\n","  ",$d);
	$d = str_replace("|","&#124;",$d);
	$d = str_replace("\\","&#92;",$d);
	$d = str_replace("(c)","&#169;",$d);
	$d = str_replace("(r)","&#174;",$d);
	$d = str_replace("\"","&#34;",$d);
	$d = str_replace("'","&#39;",$d);
	$d = str_replace("<","&#60;",$d);
	$d = str_replace(">","&#62;",$d);
	$d = str_replace("`","&#96;",$d);
	return $d;
}

function fetchurl($url) {
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $url,
		CURLOPT_SSL_VERIFYPEER  => false,
		CURLOPT_USERAGENT => 'CodeSimpleScript SSC Script HTTP_REQUEST',
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_POST => false,
		CURLOPT_CONNECTTIMEOUT => 5,
		CURLOPT_TIMEOUT => 5,
	));
	
	$return = curl_exec($curl);
	if (curl_error($curl)){
		return false;
	}
	curl_close($curl);
	return $return;
}

function heycafe_endpoint($function,$post=[]){
	//Example using post content
	//$post = [ 'content' => base64_encode("Hey look some content") ];
	//heycafe_endpoint("post_conversation_create",$post);
	
	global $system_apikey;
	$curl = curl_init();
	
	$postarray=array();
	$postarray["data"]=json_encode($post);
	
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => "https://endpoint.hey.cafe/".$function."?auth=".$system_apikey."",
		CURLOPT_SSL_VERIFYPEER  => false,
		CURLOPT_USERAGENT => 'CodeSimpleScript SSC Script HTTP_REQUEST',
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_POST => true,
		CURLOPT_CONNECTTIMEOUT => 5,
		CURLOPT_TIMEOUT => 5,
		CURLOPT_POSTFIELDS => $postarray
	));
	
	$return = curl_exec($curl);
	if (curl_error($curl)){
		return false;
	}
	curl_close($curl);
	return $return;
}

function codegenerate($length=10){
	$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}

//##############################################################
//##############################################################-- Check Notifications
//##############################################################


$notifications=heycafe_endpoint("get_account_notifications",['type' => 'mention', 'seen' => 'no']);
if ($notifications!=false){
	$mentions=json_decode($notifications,true);
	if ($mentions["system_api_error"]!=true){
		if (is_array($mentions["response_data"]["notifications"])){
			echo "<div>Got some notifications</div>";
			foreach ($mentions["response_data"]["notifications"] as $notification){
				if ($notification["seen"]=="0"){
					
					//--We have a notification, lets send a response to this mention
					echo "<div><strong>Mention:</strong> By ".$notification["from"]["name"]."</div>";
					
					//--Generate Content
					$content="Oh my, you need a password!    Your password is ** ".codegenerate(4)."-".codegenerate(8)."-".codegenerate(8)."-".codegenerate(8)." **    __ You should NEVER use this as a password! __";
					heycafe_endpoint("post_comment_create",[ 'content' => base64_encode($content), 'conversation' => $notification["resource"], 'comment' => $notification["resource_sub"] ]);
					
					//--Mark as read so wont run again
					heycafe_endpoint("post_account_notification_seen",[ 'notification' => $notification["id"] ]);
				}
			}
		}else{
			echo "<div>We have no mentions</div>";
		}
	}else{
		echo "<div>Error on notification call: ".$mentions["system_api_error"]."</div>";
	}
}